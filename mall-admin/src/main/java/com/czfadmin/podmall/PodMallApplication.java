package com.czfadmin.podmall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PodMallApplication {
	public static void main(String[] args) {
		SpringApplication.run(PodMallApplication.class, args);
	}
}
